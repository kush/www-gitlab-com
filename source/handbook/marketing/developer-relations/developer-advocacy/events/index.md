---
layout: markdown_page
title: "Developer Advocacy - Events"
---

Events are an important part of developer relations efforts. You can expect to travel a lot. Keep track of which events you go to via your calendar and make sure you take some vacation time.

## Selecting Events

TBD.

## Pre-Event

Before the event, be sure to use social channels to let people know you'll be there. If you're giving a talk, make sure to include that too. Add events to your calendar and make sure to let everyone else on the developer relations team know so they can plan accordingly.

## Travel

- Book flights that cause the least amount of agony without being over the top. A 12 hour layover to save $200 does no one any favors.
- Try to arrive the day before the event begins, and try to leave the day after. 
- Stay at the conference hotel if possible. Attendees tend to hang out there.

## At The Event

- Attend the event and be present even if you're done speaking.
- Go to talks by other speakers and tweet things you find interesting.
- Use the official event hashtag (if available) to find other attendees on Twitter.
- Retweet conference updates put out by the organizers.
- Be the first in and the last out. When attendees are starting their day or headed to an after-party, you should make an appearance.
- Get a good night's sleep the night before any presentations you give.
- If you're giving a talk, use the official hashtag to share location/time as well as your slides once you're done.

## Event Debrief

After an event, put a debrief together to help us determine whether we're hitting our goals. A short summary should answer:

- Overall feedback about the event. Was the location okay? Was it hard to get to? Was it well organized?
- Overview of who was there (Developers? C-levels? Non-technical people?)
- Were there any competitors there? What was their presence like?
- Who did you speak to that we need to follow up with? Did you meet any customer/hiring prospects?
- What went well?
- What didn't go so well?
- What can we improve?

If you gave a talk, be sure to include a [debrief](#) about that as well.

Make sure to follow up with anyone you spoke to at the event, even if it's just to say "Thanks for chatting!"

Event organizers also love to hear feedback about their events. Make sure to thank them and tell them what they did well. If things didn't go according to plan, make sure they know that too so they can incorporate your feedback next time.